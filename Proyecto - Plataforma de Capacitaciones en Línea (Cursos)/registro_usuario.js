
const form = document.getElementById('formularioRegistro'); //Formulario
const mensaje1 = document.getElementById('mensaje1'); //Mensaje de Validación
const mensaje2 = document.getElementById('mensaje2'); //Mensaje de Validación
const mensaje3 = document.getElementById('mensaje3'); //Mensaje de Validación
const mensaje4 = document.getElementById('mensaje4'); //Mensaje de Validación
const mensaje5 = document.getElementById('mensaje5'); //Mensaje de Validación
const mensaje6 = document.getElementById('mensaje6'); //Mensaje de Validación


form.addEventListener('submit', function (e) //Se agrega un evento escucha
{
    e.preventDefault();
    const nombre = document.getElementById('nombre').value;
    const email = document.getElementById('email').value;
    const telefono = document.getElementById('telefono').value;
    const usuario = document.getElementById('usuario').value;
    const password = document.getElementById('password').value;
    const password2 = document.getElementById('password2').value;

    const ExpReg_nombre = /^[a-zA-ZÀ-ÿ\s]{5,40}$/;
    const ExpReg_email = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
    const ExpReg_telefono = /^\d{10}$/;
    const ExpReg_usuario = /^[a-zA-Z0-9\_\-]{4,16}$/;
    const ExpReg_password = /^.{4,12}$/


    if (!ExpReg_nombre.test(nombre)) {
        document.getElementById("nombre").focus();
        // El formulario es válido, así que borra el mensaje de error
        mensaje1.innerHTML = "El nombre debe tener de 5 a 40 dígitos y solo puede contener letras(incluyendo acentos y espacios)";
        //Se detiene el envio del formulario
        return false;
    } else mensaje1.innerHTML = "";

    if (!ExpReg_email.test(email)) {
        document.getElementById("email").focus();
        // El formulario es válido, así que borra el mensaje de error
        mensaje2.innerHTML = "No es un correo valido";
        //Se detiene el envio del formulario
        return false;
    } else mensaje2.innerHTML = "";

    if (!ExpReg_telefono.test(telefono)) {
        document.getElementById("telefono").focus();
        // El formulario es válido, así que borra el mensaje de error
        mensaje3.innerHTML = "El telefono solo puede contener numeros y deben ser 10 dígitos";
        //Se detiene el envio del formulario
        return false;
    }
    else mensaje3.innerHTML = "";

    if (!ExpReg_usuario.test(usuario)) {

        document.getElementById("usuario").focus();
        // El formulario es válido, así que borra el mensaje de error
        mensaje4.innerHTML = "El usuario tiene que ser de 4 a 16 dígitos y solo puede contener numeros, letras, guion bajo y guion medio.";
        //Se detiene el envio del formulario
        return false;
    } else mensaje4.innerHTML = "";

    if (!ExpReg_password.test(password)) {

        document.getElementById("password").focus();
        // El formulario es válido, así que borra el mensaje de error
        mensaje5.innerHTML = "La contraseña debe ser de 4 a 16 digitos";
        //Se detiene el envio del formulario
        return false;
    } else mensaje5.innerHTML = "";

    if (password2 !== password){
        document.getElementById("password2").focus();
        mensaje6.innerHTML = "Las contraseñas son diferentes";
        return false
    } else {mensaje6.innerHTML="";
    window.location.href = 'CursosRecomendados.html';}
});