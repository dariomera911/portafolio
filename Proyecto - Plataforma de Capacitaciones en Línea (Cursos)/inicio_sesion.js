const form = document.getElementById('formularioCliente'); //Formulario
const mensaje7 = document.getElementById('mensaje7'); //Mensaje de Validación
const mensaje8 = document.getElementById('mensaje8'); //Mensaje de Validación


form.addEventListener('submit', function (e) //Se agrega un evento escucha
{
    e.preventDefault();

    var usuario = document.getElementById('usuario').value;
    var password = document.getElementById('password').value;

    // Validación de usuario y contraseña predeterminados (para propósitos de prueba)
    if (usuario !== 'Ruben') {
        document.getElementById("usuario").focus();
        mensaje7.innerHTML = "Usuario incorrecto";
        return false;
    } else mensaje7.innerHTML = "";

    if (password !== '123') {
        document.getElementById("password").focus();
        mensaje8.innerHTML = "Contraseña incorrecta.";
        return false;
    } else {
        mensaje8.innerHTML = "";
        window.location.href = 'CursosRecomendados.html';
    }
    return true;
});
