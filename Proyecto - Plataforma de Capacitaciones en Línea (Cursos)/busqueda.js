function filtrarCursos() {
    var input = document.getElementById("buscador");
    var filter = input.value.toLowerCase();
    var cursos = document.querySelectorAll(".cursos");

    cursos.forEach(function(curso) {
        var titulo = curso.querySelector("h3").textContent.toLowerCase();
        if (titulo.includes(filter)) {
            curso.style.display = "block";
        } else {
            curso.style.display = "none";
        }
    });
}