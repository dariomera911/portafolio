function calcularResultado() {
    var numero1 = parseFloat(document.getElementById('numero1').value);
    var numero2 = parseFloat(document.getElementById('numero2').value);
    var i;
    var suma;
    var resta, mult, div, mod;
    for (i = 1; i <= 5; i++) {
        switch (i) {
            case 1:
                suma = numero1 + numero2;
                break;
            case 2:
                resta = numero1 - numero2;
                break;
            case 3:
                mult = numero1 * numero2;
                break;
            case 4:
                if (numero2 !== 0) {
                    div = numero1 / numero2;
                } else {
                    div = "División por cero no permitida";
                }
                break;
            case 5:
                if (numero2 !== 0) {
                    mod = numero1 % numero2;
                } else {
                    mod = "División por cero no permitida";
                }

                break;

            default:
                resultado = "Operación no válida";
        }
    }
    document.getElementById("suma").textContent = suma;
    document.getElementById("resta").textContent = resta;
    document.getElementById("mult").textContent = mult;
    document.getElementById("division").textContent = div;
    document.getElementById("mod").textContent = mod;
}